# GitLab config

GitLab configuration files

## Installation

Assuming Node.js and npm are already installed:
```bash
npm install
```

An authentication token is required to run this tool:
```bash
# Using env
export ACCESS_TOKEN=xxx

# Using a file:
echo '"xxx"' > .token.json
```

## Running

To run the configuration scripts:
```bash
# Remove the dry-run part to actually update the configuration
npx gitlab-config --dry-run --debug

# Some options are available:
npx gitlab-config --help
```

## Documentation

For details about configuration format and available modules, please refer to [gitlab-config](https://gitlab.cern.ch/mro/common/tools/gitlab-config/-/blob/master/README.md)
