module.exports = {
  path: [
    'apc/common'
  ],
  settings: [
    {
      plugin: 'schedules',
      excludeAll: true,
      exclude: [
        { path: 'gitlab-config-apc' },
        { path: 'apc-dev-proxy' },
        { path: 'docker-image-susoft-android' },
        { path: 'docker-image-susoft-cpp' }
      ],
      include: [
        { namespace: { full_path: 'apc/common/tools' } },
        { namespace: { full_path: 'apc/common/www' } },
        { namespace: { full_path: 'apc/common' } }
      ],
      config: {
        prefix: '[auto] ',
        schedules: [
          {
            description: 'Weekly schedule',
            cron: '42 0 * * 0',
            cron_timezone: 'Europe/Paris',
            active: true,
            ref: 'master'
          }
        ]
      }
    },
    {
      service: 'mattermost',
      plugin: 'integrations',
      exclude: [],
      include: [],
      config: {
        active: true,
        push_events: false, issues_events: true,
        confidential_issues_events: false,
        merge_requests_events: true,
        tag_push_events: true,
        note_events: false,
        confidential_note_events: false,
        pipeline_events: true,
        wiki_page_events: false,
        job_events: false,
        webhook: 'https://mattermost.web.cern.ch/hooks/i4rcha7jqpdb3jm5bswafwxxyh',
        username: '',
        notify_only_broken_pipelines: true,
        branches_to_be_notified: 'default_and_protected',
        push_channel: '',
        issue_channel: '',
        confidential_issue_channel: '',
        merge_request_channel: '',
        note_channel: '',
        tag_push_channel: '',
        pipeline_channel: '',
        wiki_page_channel: ''
      }
    },
    /*{
      service: 'jira',
      plugin: 'integrations',
      exclude: [],
      include: [],
      config: {
        active: true,
        commit_events: true, // Jira comments will be created when an issue gets referenced in a commit.
        merge_requests_events: true, // Jira comments will be created when an issue gets referenced in a merge request.
        comment_on_event_enabled: true // Comment will be posted on each event
      }
    },*/
    {
      plugin: 'gitlab-ci-badges',
      exclude: [],
      include: []
    },
    {
      plugin: 'custom-badges',
      exclude: [],
      include: [],
      config: {
        badges: {
          lint: {
            link_url: 'http://apc-dev.web.cern.ch/ci/%{project_path}/lint_result.html',
            image_url: 'http://apc-dev.web.cern.ch/ci/%{project_path}/lint_badge.svg'
          },
          flow: {
            link_url: 'http://apc-dev.web.cern.ch/ci/%{project_path}/flow',
            image_url: 'http://apc-dev.web.cern.ch/ci/%{project_path}/flow/flow-badge.svg'
          },
          flow_coverage: {
            link_url: 'http://apc-dev.web.cern.ch/ci/%{project_path}/flow',
            image_url: 'http://apc-dev.web.cern.ch/ci/%{project_path}/flow/flow-coverage-badge.svg'
          },
          git_badge: {
            link_url: 'http://gitlab.cern.ch/%{project_path}/',
            image_url: 'http://apc-dev.web.cern.ch/ci/%{project_path}/git_badge.svg'
          },
          tc_badge: {
            link_url: 'http://apc-dev.web.cern.ch/ci/%{project_path}/tc_result.html',
            image_url: 'http://apc-dev.web.cern.ch/ci/%{project_path}/tc_badge.svg'
          },
          tc_coverage: {
            link_url: 'http://apc-dev.web.cern.ch/ci/%{project_path}/tc/index.html',
            image_url: 'http://apc-dev.web.cern.ch/ci/%{project_path}/tc_coverage.svg'
          }
        }
      }
    },
    {
      plugin: 'git-submodules',
      exclude: [],
      include: []
    },
    { /* tools */
      plugin: 'members',
      excludeAll: true,
      include: [{ name: 'gitlab-config-apc' }],
      config: { maintainer: [ 'sfargier' ] }
    },
    { /* must be done after "members" rules */
      plugin: 'merge-request',
      exclude: [],
      include: [],
      config: {
        merge_requests_enabled: true,
        only_allow_merge_if_pipeline_succeeds: true,
        only_allow_merge_if_all_discussions_are_resolved: true,
        merge_method: "rebase_merge", // "merge"|"rebase_merge"|"ff",
        printing_merge_request_link_enabled: true,
        resolve_outdated_diff_discussions: false,
        approvers: "maintainer", // Array<username>|"owner"|"maintainer"|"developer"|"reporter"|"guest"
        approvals: {
          approvals_before_merge: 1,
          reset_approvals_on_push: true,
          disable_overriding_approvers_per_merge_request: true,
          merge_requests_author_approval: true
        }
      }
    },
    {
      plugin: 'protected-tags',
      config: {
        tags: [
          { name: 'v*', access_level: 'maintainer' }
        ]
      }
    },
    {
      plugin: 'protected-branches',
      config: {
        branches: [
          { name: 'master', merge_access_level: 'maintainer', push_access_level: 'maintainer' }
        ]
      }
    },
    {
      plugin: 'deploy-keys',
      exclude: [
        { path: 'apc-dev-proxy' },
        { path: 'gitlab-ci-report' }, { path: 'x-builder' } // mirrors
      ],
      config: {
        keys: [
          { title: 'apcdev-deploy-key',
            key: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC28Bl77kEFW3L2BJX5mLUFHroPf9qY9jxq8hOHBxaWAmgafGShpMc8RUGV5Upxifl+uCJ1NqwyNK8rASj9GMtcMUtY1/HiC/Dv6rxrLDtios8sYZ4lBaWDiyMIx7NdfpHQJ7pSUmspaPp7InYObLSZKPYbWzLLUlEXbHpWYYIhphzrv2Kqj59WRue94syVvmmYRUq2pfybWZCfiliED4JDEUINMJYCfhw2epKT4Ry99tnVACeJ7+I0UxMsWedU258ZJFBHMRe807l/qfupMOx3TIrbphz84sFINJ7wk8pGscvpU4QKMEvitwd0cPaSL2cMOnDlHYYGS8KlbaJphslECRzgq22qSjHmQ/TEsVyTthicreN0NUZSn0+RrfUoYcbvgLC0IotgaZvYDwhBW0RjLwY5rQdzSWdkDH5ikmj5ASKomBbCEsDGe6/cmBUbrUYmakUlAAnF0JknnQrAXmmaVACnuUIRXgThiZC2r23i8Ijj7w5DlUCxwGphd4kUvoE= apcdev@cern.ch',
            can_push: true }
        ]
      }
    },
    { /* mirrors */
      plugin: 'deploy-keys',
      excludeAll: true,
      include: [ { path: 'gitlab-ci-report' }, { path: 'x-builder' } ],
      config: {
        keys: [
          { title: 'apcdev-deploy-key',
            key: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC28Bl77kEFW3L2BJX5mLUFHroPf9qY9jxq8hOHBxaWAmgafGShpMc8RUGV5Upxifl+uCJ1NqwyNK8rASj9GMtcMUtY1/HiC/Dv6rxrLDtios8sYZ4lBaWDiyMIx7NdfpHQJ7pSUmspaPp7InYObLSZKPYbWzLLUlEXbHpWYYIhphzrv2Kqj59WRue94syVvmmYRUq2pfybWZCfiliED4JDEUINMJYCfhw2epKT4Ry99tnVACeJ7+I0UxMsWedU258ZJFBHMRe807l/qfupMOx3TIrbphz84sFINJ7wk8pGscvpU4QKMEvitwd0cPaSL2cMOnDlHYYGS8KlbaJphslECRzgq22qSjHmQ/TEsVyTthicreN0NUZSn0+RrfUoYcbvgLC0IotgaZvYDwhBW0RjLwY5rQdzSWdkDH5ikmj5ASKomBbCEsDGe6/cmBUbrUYmakUlAAnF0JknnQrAXmmaVACnuUIRXgThiZC2r23i8Ijj7w5DlUCxwGphd4kUvoE= apcdev@cern.ch',
            can_push: true },
          { title: 'mro-deploy-key', can_push: true,
            key: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC43hEEoNogXRfuagx86TYXIwb1fxb3NxrRsdoYHAsQzKZKPvP8xacOCcIJ+eK+Ee1sIXdfD6Gy/yp7g1/qEzSIf7qFYGG0nbexWeYiDXOmpKVcy2bR+CWk8SVtFitxLByAi4gXwh8zaCsZdfosJamPz2ZS+jw5s+UQt4DojZhNKVx2+1GPu4waYyU/rDwWvz/9hCHmEhZ8lcGplabiKvxcrGbAFlGBw4YONNIJqSiDMj0fGmmq7+CMceSpgLpJsuCIZgEZIn/hVNZKRlbpATAVSynCwwY6PM2j6UaF1wM1ys5PRMPRIA4fzC2l941MOqcWoTWmGWq7evZvGOl5bmS3YgvD26DBTGZVeRfj0UF7Gr63BZn84K1S6KxUV3CPSrtCMdW6bKpmjIigdEIL6go/B59WTHLQhZifGCR74YEZ5n2IWH1W6uPf+GoYYAfOpNytNDES08DhCc1jLirZOCnXkiFQnCb6fZd2igB4d/iQsGLK4tofnI5UJFmO//8k4/8= mrodev@cern.ch' }
        ]
      }
    },
  ]
};
